#requires AutoHotkey v1.1
#SingleInstance,Force
#NoEnv

strVersion := "0.2"
strAppName := "Pomodoro"
strAppNameVersion := strAppName . " v" . strVersion
SysGet, arrMonitorWorkArea, MonitorWorkArea

intMinutesDefault := 25

intNagMax := 5
intNagInterval := 60000 ; 1 minute
blnNagVisual:= true

blnNagSound := true
; Sound #1 Short
strNagSound := "100@261.63|100@233.081"
intSoundTempo := 2.5
intSoundPitch := 1
; Sound #2 E.T.
; strNagSound := "450@586|450@658|450@522|675@260|1800@392"
; intSoundTempo := 1
; intSoundPitch := 1
; Sound #3 - Get Up, Stand Up
; strNagSound := "100@233.081|100@261.63|100@0|100@293.66|100@311.13|100@0|600@0|100@233.081|200@261.63|200@311.13|100@233.081|200@261.63"
; intSoundTempo := 2.5
; intSoundPitch := 1

Menu, Tray, Add, Minutes remaining, MinutesRemaining
Menu, Tray, Add, Restart %strAppName%, Restart
Menu, Tray, Tip, %strAppNameVersion%
Menu, Tray, Icon, %A_ScriptDir%\bell-C-344x236.ico

Gui, 1:New, +hwndintGuiId -MaximizeBox -MinimizeBox, %strAppName%
Gui, Font, s10
Gui, Add, Text, x10, Next pause?
Gui, Add, Edit, x10 center number vf_intMinutes, %intMinutesDefault%
Gui, Add, Text, yp+5 x+5, minutes
Gui, Font, s9, Arial
Gui, Add, Button, yp-5 x+5 Default gStartTimer, % chr(0x25BA) ; Go
Gui, Font, s10
Gui, Add, Picture, x30 y10 w75 h51 hidden vf_picBellL AltSubmit +BackgroundTrans, %A_ScriptDir%\bell-L-344x236.gif
Gui, Add, Picture, x30 y10 w75 h51 hidden vf_picBellC AltSubmit +BackgroundTrans, %A_ScriptDir%\bell-C-344x236.gif
Gui, Add, Picture, x30 y10 w75 h51 hidden vf_picBellR AltSubmit +BackgroundTrans, %A_ScriptDir%\bell-R-344x236.gif
Gui, Show, % "x" . A_ScreenWidth - 300 . " y" . A_ScreenHeight - 200
WinGetPos, , , intGuiWidth, intGuiHeight, ahk_id %intGuiId%

return

StartTimer:
SetTimer, Nag, Off
Gui, Submit, NoHide
intMinutes := f_intMinutes
if !(intMinutes > 0)
	return
Gui, Hide
strStarted := A_Now
Loop, %intMinutes%
{
	Menu, Tray, Tip, %intMinutes% minutes remaining`n%strAppNameVersion%
	intMinutes--
	Sleep, 60000 ; 1 minute
}
Gui, Show, % "NoActivate x" . arrMonitorWorkAreaRight - intGuiWidth - 10 . " y" . arrMonitorWorkAreaBottom - intGuiHeight - 10
WinSet, AlwaysOnTop, On, ahk_id %intGuiId%
SetTimer, Nag, % intNagInterval * 1000 ; run again after interval
SetTimer, NagOnce, -1 ; run once now
return

MinutesRemaining:
strFinish := strStarted
EnvAdd, strFinish, intMinutes, Minutes
EnvSub, strFinish, A_Now, minutes
MsgBox, %strFinish% minutes remaining...
return

Nag:
NagOnce:
intNagCounter++
; ToolTip, % A_ThisLabel . "`n" . A_Now . "`n" . intNagCounter . "`n" . intNagMax . "`n" . WinActive("ahk_id " . intGuiId)
if (intNagCounter > intNagMax or WinActive("ahk_id " . intGuiId))
{
	SetTimer, Nag, Off
	Sleep, 2000
	ToolTip
	return
}
if (blnNagVisual)
	loop, 5
		loop, Parse, % "CLCR"
		{
			GuiControl, 1:Show, f_picBell%A_LoopField%
			Sleep, 100
			GuiControl, 1:Hide, f_picBell%A_LoopField%
			Menu, Tray, Icon, %A_ScriptDir%\bell-%A_LoopField%-344x236.ico
		}
	Menu, Tray, Icon, %A_ScriptDir%\bell-C-344x236.ico
if (blnNagSound and !WinActive("ahk_id " . intGuiId))
	Gosub, PlaySound ; SetTimer, PlaySound, -1
return

PlaySound:
Loop, Parse, % strNagSound, |
{
	saThisSound := StrSplit(A_LoopField, "@") ; 1 duration, 2 note
	SoundBeep, % saThisSound[2] * intSoundPitch, % (saThisSound[1] * intSoundTempo)
}
return

Restart:
Reload
return

GuiCancel:
Gui Destroy
return

